# Master thesis


## Webová služba pro optimalizaci marketingových kampaní na sociálních sítích
Cílem je vyvinout webovou službu, která umožňuje monitorovat vhodné metriky na sociálních sítích za účelem optimalizace marketingových kampaní. Služba by měla být navržena pro marketingové agentury a vlastníky značek, kteří mají interní marketingové oddělení. Klíčovým přínosem sledování metrik pomocí této služby je optimalizace výdajů na marketingové kampaně přes sociální sítě Facebook, Instagram a Twitter.

- Analyzujte možnosti získávání dat ze sociálních sítí Facebook, Instagram a Twitter.
- Navrhněte a popište vhodné metriky pro sledování a optimalizaci rozpočtu na marketingovou kampaň.
- Proveďte rešerši existujících řešení.
- Navrhněte vlastní řešení formou SaaS, které bude podporovat sledování navržených metriky včetně reportingu.
- Implementujte dle návrhu s využitím technologií .NET 6 pro backend aplikace a Angular pro frontend. Implementaci také vhodnou formou otestujte.
- Řešení zdokumentujte a umožněte nasazení pomocí technologie Docker.
- Zhodnoťte ekonomicko-manažerské přínosy vlastního řešení, porovnejte s řešeními z rešerše a navrhněte další rozvoj.


## Harmonogram

Datum dokončení
Předpokládám někdy na začátku ledna 2024 bude odevzdání práce - dávám si tedy prostor do konce roku 2023
31.12.2023


### Analýza -  1.7.2023 - 1.8.2023
- Úvod do problematiky
- Možnosti získávání dat na sociálních sítích
- Potřeby
    - Tady by možná bylo dobré najít nějakou firmu, se kterou bych to konzultoval, ale nevím, jestli se mi to povede
- Metriky
- Funkční / nefunkční požadavky

### Rešerše - 1.8.2023 - 20.8.2023
- Řešení na trhu

### Návrh řešení - 20.8.2023 - 1.11.2023
- Případy užití
- Architektura
- UI

### Realizace 20.8.2023 - 1.11.2023
- Implementace
- Testy
    - Unit a integrační testy
- DevOps

(Pozn. architekturu a implementaci obecných věcí jako je třeba autentizace atd. chci dělat již v letním semestru -> 1.3.2023 - 1.7.2023)

### Ekonomicko-manažerské zhodnocení 1.11.2023 - 15.12.2023
- Manažerské zhodnocení - přínosy pro zákazníka
- Ekonomické zhodnocení - náklady, provoz, business case?
    - Z pohledu vlastníka SaaS?
    - Z pohledu zákazníka?
- Další rozvoj
